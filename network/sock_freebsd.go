// +build freebsd
/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package network

import (
	"os"
	"syscall"
)

func SockSetFIB(fd int, fib int) error {
	return os.NewSyscallError("socksetfib", syscall.SetsockoptInt(fd, syscall.SOL_SOCKET, syscall.SO_SETFIB, fib))
}
