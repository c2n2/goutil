/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package network

import (
	"fmt"
	"net"
	"os"
	"strconv"
	"syscall"
)

func TCPSock() (fd int, err error) {
	return syscall.Socket(syscall.AF_INET, syscall.SOCK_STREAM, syscall.IPPROTO_TCP)
}

func UDPSock() (fd int, err error) {
	return syscall.Socket(syscall.AF_INET, syscall.SOCK_DGRAM, syscall.IPPROTO_UDP)
}

func AddrToSockaddr(addr *net.TCPAddr) (syscall.Sockaddr, error) {
	switch {
	case addr.IP.To4() != nil:
		ip := [4]byte{}
		copy(ip[:], addr.IP.To4())
		return &syscall.SockaddrInet4{Addr: ip, Port: addr.Port}, nil
	default:
		ip := [16]byte{}
		copy(ip[:], addr.IP.To16())
		zoneID, err := strconv.ParseUint(addr.Zone, 10, 32)
		if err != nil { return nil, err }
		return &syscall.SockaddrInet6{Addr: ip, Port: addr.Port, ZoneId: uint32(zoneID)}, nil
	}
}

func SockBind(fd int, sa syscall.Sockaddr) error {
	if s, ok := sa.(interface{}).(syscall.SockaddrInet4); ok {
		fmt.Print(s.Addr, s.Port)
	}



	return os.NewSyscallError("sockbind", syscall.Bind(fd, sa))
}

func SockConnect(fd int, sa syscall.Sockaddr) (sock net.Conn, err error) {
	err = os.NewSyscallError("sockconnect", syscall.Connect(fd, sa))
	if err != nil {return nil, err}
	fs := os.NewFile(uintptr(fd), "");
	defer fs.Close()
	sock, err = net.FileConn(fs)
	if err !=nil {
		return nil, os.NewSyscallError("sockfilecon", err)
	} else {
		return sock, nil
	}
}
