/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package postgres

import (
	"testing"

	"gitlab.com/c2n2/goutil/logger"
	"gitlab.com/c2n2/goutil/stuff"
)

func TestGetSchema(t *testing.T) {
	schema, err := getSchema()
	if err != nil {
		logger.Errorf("Failed to get schema: %v", err)
	}
	stuff.TabSymbol = "  "
	t.Log(stuff.PrettyJson(schema))
}

func init()  {
	logger.SetLevel("debug")
	logger.EnableColors()
}