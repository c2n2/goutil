/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package postgres

import (
	"io/ioutil"
	"path"
	"strconv"
	"strings"

	"github.com/jawher/mow.cli"

	"gitlab.com/c2n2/goutil/logger"
)

type MigrateAction uint8

const (
	UpgradeMigration MigrateAction = 0 + iota
	DowngradeMigration
)

type migration struct {
	name, fname, fpath string
	version            int
	action             MigrateAction
}

func newMigration(fname, fpath string) *migration {
	return &migration{fname: fname, fpath: fpath}
}

func (m *migration) init() *migration {
	//
	var (
		ver int64
		err error
	)
	// split fname
	sn := strings.Split(m.fname, "_")
	if len(sn) != 2 {
		return nil
	}
	// set version
	if ver, err = strconv.ParseInt(sn[0], 10, 64); err != nil {
		return nil
	}
	m.version = int(ver)
	// split name action part
	sa := strings.Split(sn[1], ".")
	if len(sa) != 3 {
		return nil
	}
	// set action
	switch sa[1] {
	case "down":
		m.action = DowngradeMigration
	case "up":
		m.action = UpgradeMigration
	default:
		return nil
	}
	// set name
	m.name = sa[0]

	return m
}

/// Commands

func Migrate(cmd *cli.Cmd) {

}

func createMigration(cmd *cli.Cmd) {

}

/// Stuff

func findMigrations(rpath string) {
	mmap := make(map[int]*migration)
	content, err := ioutil.ReadDir(rpath)
	if err != nil {
		logger.Panicf("Filed to search migrations files: %v", err)
	}
	//
	for _, f := range content {
		fpath := path.Join(rpath, f.Name())
		if path.Ext(fpath) == "sql" {
			if m := newMigration(f.Name(), fpath).init(); m != nil {
				mmap[m.version] = m
			}
		}
	}
}
