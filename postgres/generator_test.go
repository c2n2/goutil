/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package postgres

import (
	"testing"

	"gitlab.com/c2n2/goutil/logger"
)

func TestGenerateModels(t *testing.T) {
	logger.SetLevel("debug")
	GenerateModels("./", "postgres")
}
