/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package postgres

import (
	"github.com/go-pg/pg"
	"gitlab.com/c2n2/goutil/logger"
	"gitlab.com/c2n2/goutil/stuff"
	"net/url"
)

/*
Zerro conf environ variables:
- PSQL_ENTRYPOINT: URL format postgres://user:password@host:port/database?[optname=value[&optnameN=value]]
*/

const (
	DefaultEntryPoint = "postgres://tester:tester@localhost:5432/tester"
)

var (
	client *pg.DB = new_client()
	// return transaction
	Begin = client.Begin
	Model = client.Model
	Insert = client.Insert
	Select = client.Select
	Update = client.Update
	Exec = client.Exec
	Query = client.Query
)

type pgDebugger struct {}

func (d *pgDebugger) BeforeQuery(e *pg.QueryEvent) {
	if q, err := e.FormattedQuery(); err == nil {
		logger.Debug(q)
	}
}

func (d *pgDebugger) AfterQuery(e *pg.QueryEvent) {

}

func new_client() *pg.DB {
	// parse entrypoint
	dsn, err := url.Parse(
		stuff.EnvOrDefault("PSQL_ENTRYPOINT", DefaultEntryPoint),
	)
	if err != nil {
		logger.Fatalf("Failed to parse Postgres entrypoint: %v", err)
	}
	//
	options := &pg.Options{
		Network: "tcp",
		Addr: dsn.Host,
		User: dsn.User.Username(),
		Database: dsn.RawPath,
	}
	//
	if passwd, ok := dsn.User.Password(); ok {
		options.Password = passwd
	}
	// create client
	return pg.Connect(options)
}

func init() {
	client.AddQueryHook(&pgDebugger{})
}