/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package stuff

import (
	"bytes"
	"encoding/json"
)

var (
	EmptySymbol = ""
	TabSymbol = "\t"
)

func PrettyJson(data interface{}) (string, error) {
	buffer := new(bytes.Buffer)
	encoder := json.NewEncoder(buffer)
	encoder.SetIndent(EmptySymbol, TabSymbol)

	err := encoder.Encode(data)
	if err != nil {
		return EmptySymbol, err
	}
	return buffer.String(), nil
}