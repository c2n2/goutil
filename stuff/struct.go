/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package stuff

import (
	"fmt"
	"strings"
)

// Tagging struct fields extension
type FieldExtension struct {
	tags []tag
}

func (e *FieldExtension) AddTag(abbr, val string) *FieldExtension {
	for i, t := range e.tags {
		if t.abbr == abbr {
			e.tags[i].members = append(e.tags[i].members, val)
			return e
		}
	}
	e.tags = append(e.tags, tag{abbr: abbr, members: []string{val}})
	return e
}

func (e *FieldExtension) String() string {
	slice := make([]string, 0)
	for _, t := range e.tags {
		slice = append(slice, fmt.Sprintf(`%s:"%s"`, t.abbr, t.String()))
	}
	return "`" + strings.Join(slice, " ") + "`"
}

type tag struct {
	abbr    string
	members []string
}

func (t tag) String() string {
	return strings.Join(t.members, ",")
}

func NewFieldExtension() *FieldExtension {
	return &FieldExtension{}
}