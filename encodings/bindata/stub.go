/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package bindata

import (
	"unsafe"
)

func memdump(v interface{}) (slice []byte) {
	vptr, size := inspect_t(v)
	slice = make([]byte, size)

	ptr := uintptr(vptr)
	for i := 0; i < len(slice); i++ {
		slice[i] = byte(*(*byte)(unsafe.Pointer(ptr)))
		ptr++
	}

	return
}

func memset(dst unsafe.Pointer, src []byte) {
	ptr := uintptr(dst)
	for i := 0; i < len(src); i++ {
		*(*byte)(unsafe.Pointer(ptr)) = src[i]
		ptr++
	}
}