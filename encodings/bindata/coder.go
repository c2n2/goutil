/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package bindata

import (
	"fmt"
	"unsafe"
)

func inspect_t(v interface{}) (unsafe.Pointer, uintptr) {
	// inspect value
	switch t := v.(type) {
	case bool:
		i := v.(bool)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case int8:
		i := v.(int8)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case int16:
		i := v.(int16)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case int32:
		i := v.(int32)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case int64:
		i := v.(int64)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case int:
		i := v.(int)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case uint8:
		i := v.(uint8)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case uint16:
		i := v.(uint16)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case uint32:
		i := v.(uint32)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case uint64:
		i := v.(uint64)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case uint:
		i := v.(uint)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case uintptr:
		i := v.(uintptr)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case float32:
		i := v.(float32)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case float64:
		i := v.(float64)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case complex64:
		i := v.(complex64)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case complex128:
		i := v.(complex128)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	case string:
		i := v.(string)
		return unsafe.Pointer(&i), unsafe.Sizeof(i)
	default:
		panic(fmt.Errorf("Unsupported type: %T\n", t))
	}
}

/// builtin decoders

func Bool(b []byte) (bool, error) {
	v := new(bool)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Int(b []byte) (int, error) {
	v := new(int)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Int8(b []byte) (int8, error) {
	v := new(int8)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Int16(b []byte) (int16, error) {
	v := new(int16)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Int32(b []byte) (int32, error) {
	v := new(int32)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Int64(b []byte) (int64, error) {
	v := new(int64)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Uint(b []byte) (uint, error) {
	v := new(uint)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Uint8(b []byte) (uint8, error) {
	v := new(uint8)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Uint16(b []byte) (uint16, error) {
	v := new(uint16)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Uint32(b []byte) (uint32, error) {
	v := new(uint32)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Uint64(b []byte) (uint64, error) {
	v := new(uint64)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Uintptr(b []byte) (uintptr, error) {
	v := new(uintptr)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Float32(b []byte) (float32, error) {
	v := new(float32)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Float64(b []byte) (float64, error) {
	v := new(float64)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Complex64(b []byte) (complex64, error) {
	v := new(complex64)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func Complex128(b []byte) (complex128, error) {
	v := new(complex128)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}

func String(b []byte) (string, error) {
	v := new(string)
	memset(unsafe.Pointer(v), b)
	return *v, nil
}