/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package curs

import (
	"gitlab.com/c2n2/goutil/encodings/ansi"
)

func Up(n int) string {
	return ansi.ControlEscape(ansi.CUU, n)
}

func Down(n int) string {
	return ansi.ControlEscape(ansi.CDU, n)
}

func Fwd(n int) string {
	return ansi.ControlEscape(ansi.CUF, n)
}

func Back(n int) string {
	return ansi.ControlEscape(ansi.CUB, n)
}

func NextL(n int) string {
	return ansi.ControlEscape(ansi.CNL, n)
}

func PrevL(n int) string {
	return ansi.ControlEscape(ansi.CPL, n)
}

func AbsH(n int) string {
	return ansi.ControlEscape(ansi.CHA, n)
}

func Pos(line, col int) string {
	return ansi.ControlEscape(ansi.CUP, line, col)
}

func EraseD(n int) string {
	return ansi.ControlEscape(ansi.ED, n)
}

func EraseL(n int) string {
	return ansi.ControlEscape(ansi.EL, n)
}

func ScrlUp(n int) string {
	return ansi.ControlEscape(ansi.SU, n)
}

func ScrlDown(n int) string {
	return ansi.ControlEscape(ansi.SD, n)
}

func Pos2D(x, y int) string {
	return ansi.ControlEscape(ansi.HVP, x, y)
}

func SavePos() string {
	return ansi.ControlEscape(ansi.SCP)
}

func RestPos() string {
	return ansi.ControlEscape(ansi.RCP)
}

func Show() string {
	return ansi.ControlEscape(ansi.CSHOW)
}

func Hide() string {
	return ansi.ControlEscape(ansi.CHIDE)
}