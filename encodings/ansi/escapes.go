/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

/*
see http://www.ecma-international.org/publications/standards/Ecma-048.htm
*/

package ansi

import (
	"fmt"
)

const (
	Esc = "\x1b"
	Fg = "[38;5;%d"

)

// CSI (Control Sequence Introducer) sequences
const (
	CUU   = "[%dA"    // Cursor Up
	CDU   = "[%dB"    // Cursor Down
	CUF   = "[%dC"    // Cursor Forward
	CUB   = "[%dD"    // Cursor Back
	CNL   = "[%dE"    // Cursor Next Line
	CPL   = "[%dF"    // Cursor Previous Line
	CHA   = "[%dG"    // Cursor Horizontal Absolute
	CUP   = "[%d;%dH" // Cursor Position
	ED    = "[%dJ"    // Erase in Display
	EL    = "[%dK"    // Erase in Line
	SU    = "[%dS"    // Scroll Up
	SD    = "[%dT"    // Scroll Down
	HVP   = "[%d;%df" // Horizontal Vertical Position
	SCP   = "[s"      // Save Cursor Position
	RCP   = "[u"      // Restore Cursor Position
	CSHOW = "[?25h"   // Shows the curs
	CHIDE = "[?25l"   // Hides the curs
)

// SGR (Select Graphic Rendition) sequences


///

func ControlEscape(format string, args ...interface{}) string {
	return fmt.Sprintf("%s%s", Esc, fmt.Sprintf(format, args...))
}