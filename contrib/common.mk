# Environment
SHELL=/bin/bash
.SHELLFLAGS = -o pipefail -c

# Golang staff
GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GOMOD=$(GOCMD) mod
GOCLEAN=$(GOCMD) clean -chahe -modcache

define start-container
	$(shell [[ `docker container inspect $(1) &>/dev/null; echo $$?` == 0 ]] && docker start $(1))
endef

define stop-container
	$(shell [[ `docker container inspect $(1) &>/dev/null; echo $$?` == 0 ]] && docker stop $(1))
endef

define remove-container
	$(shell [[ `docker container inspect $(1) &>/dev/null; echo $$?` == 0 ]] && docker rm -f $(1))
endef

define create-network
	$(shell [[ $(docker network inspect $(1) &>/dev/null; echo $?) == 1 ]] && docker network create $(1))
endef

define delete-network
	$(shell [[ $(docker network inspect $(1) &>/dev/null; echo $?) == 0 ]] && docker network rm $(1))
endef

# Docker
.PHONY: docker-clear
docker-clear:
	@echo $(shell echo -e 'y' | docker system prune -a --volumes)
