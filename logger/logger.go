/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package logger

import (
	"fmt"
	"io"
	"os"
	"path"
	"runtime"
	"strings"
	"time"

	"github.com/mgutz/ansi"
)

type LogLevel uint8

const (
	DebugLevel LogLevel = 0 + iota
	InfoLevel
	WarnLevel
	ErrorLevel
	FatalLevel
	PanicLevel
)

const (
	DefaultLevel = "info"
)

var (
	logger *Logger
)

func (l LogLevel) color() string {
	return []string{"black+h", "cyan", "yellow", "red+h", "red", "red"}[l]
}

func (l LogLevel) Prefix(usecolor bool) string {
	prefix := []string{"DEBUG", "INFO", "WARN", "ERROR", "FATAL", "PANIC"}[l]
	if usecolor {
		return ansi.Color(prefix, l.color())
	}
	return prefix
}

type Logger struct {
	Facility string
	Level    LogLevel

	useColors, useCaller, useTimestamp bool
	outwriter                             io.Writer
	errwriter                             io.Writer
}

func (l *Logger) log(level LogLevel, logmsg string) {
	if l.Level <= level {
		// writer
		var writer io.Writer
		// message strings
		message := []string{}
		// check timestamp
		if l.useTimestamp {
			message = append(message, fmt.Sprintf("%v", time.Now().Local()))
		}
		// check facility
		if l.Facility != "" {
			message = append(message, fmt.Sprintf("[%s]", l.Facility))
		}
		// add prefix, message
		message = append(message, level.Prefix(l.useColors), logmsg)
		// add caller info
		if l.useCaller {
			message = append(message, fmt.Sprintf("\n\t%s", callerFmt(4)))
		}
		// write message
		bytes := []byte(strings.Join(message, " ") + "\n")
		if level >= ErrorLevel {
			writer = l.errwriter
		} else {
			writer = l.outwriter
		}
		if _, err := writer.Write(bytes); err != nil {
			panic(err)
		}
	}
}

/// settings

func SetFacility(facility string) {
	logger.Facility = facility
}

/*
SetLevel. Setting logger level
Levels can be: debug, info, warn, warning, error, fatal, panic
*/
func SetLevel(lvl string) {
	logger.Level = getLogLevel(lvl)
}

func SetOutput(writers ...io.Writer) {
	logger.outwriter = io.MultiWriter(writers...)
}

func SetErrOutput(writers ...io.Writer) {
	logger.errwriter = io.MultiWriter(writers...)
}

func EnableColors() {
	logger.useColors = true
}

func EnableCallers() {
	logger.useCaller = true
}

func EnableTimestamp() {
	logger.useTimestamp = true
}

///

func Debugf(format string, args ...interface{}) {
	logger.log(DebugLevel, messageFmt(format, args...))
}

func Infof(format string, args ...interface{}) {
	logger.log(InfoLevel, messageFmt(format, args...))
}

func Warnf(format string, args ...interface{}) {
	logger.log(WarnLevel, messageFmt(format, args...))
}

func Warningf(format string, args ...interface{}) {
	logger.log(WarnLevel, messageFmt(format, args...))
}

func Errorf(format string, args ...interface{}) {
	logger.log(ErrorLevel, messageFmt(format, args...))
}

func Fatalf(format string, args ...interface{}) {
	logger.log(FatalLevel, messageFmt(format, args...))
	os.Exit(1)
}

func Panicf(format string, args ...interface{}) {
	msg := messageFmt(format, args...)
	logger.log(PanicLevel, msg)
	panic(msg)
}

func Debug(args ...interface{}) {
	logger.log(DebugLevel, fmt.Sprint(args...))
}

func Info(args ...interface{}) {
	logger.log(InfoLevel, fmt.Sprint(args...))
}

func Warn(args ...interface{}) {
	logger.log(WarnLevel, fmt.Sprint(args...))
}

func Warning(args ...interface{}) {
	logger.log(WarnLevel, fmt.Sprint(args...))
}

func Error(args ...interface{}) {
	logger.log(ErrorLevel, fmt.Sprint(args...))
}

func Fatal(args ...interface{}) {
	logger.log(FatalLevel, fmt.Sprint(args...))
	os.Exit(1)
}

func Panic(args ...interface{}) {
	msg := fmt.Sprint(args...)
	logger.log(PanicLevel, msg)
	panic(msg)
}

func Debugln(args ...interface{}) {
	logger.log(DebugLevel, fmt.Sprintln(args...))
}

func Infoln(args ...interface{}) {
	logger.log(InfoLevel, fmt.Sprintln(args...))
}

func Warnln(args ...interface{}) {
	logger.log(WarnLevel, fmt.Sprintln(args...))
}

func Warningln(args ...interface{}) {
	logger.log(WarnLevel, fmt.Sprintln(args...))
}

func Errorln(args ...interface{}) {
	logger.log(ErrorLevel, fmt.Sprintln(args...))
}

func Fatalln(args ...interface{}) {
	logger.log(FatalLevel, fmt.Sprintln(args...))
	os.Exit(1)
}

func Panicln(args ...interface{}) {
	msg := fmt.Sprintln(args...)
	logger.log(PanicLevel, msg)
	panic(msg)
}

///

func messageFmt(format string, args ...interface{}) string {
	if len(args) != strings.Count(format, "%") {
		panic("Wrong format string! Count of verbs and args not equal.")
	}
	return fmt.Sprintf(format, args...)
}

func callerFmt(skip int) string {
	var pcs [10]uintptr
	n := runtime.Callers(skip, pcs[:])
	frames := runtime.CallersFrames(pcs[:n])
	if frame, ok := frames.Next(); ok {
		return fmt.Sprintf("method: %s() at %s:%d",
			strings.Split(frame.Func.Name(), ".")[2],
			strings.Replace(frame.File, path.Join(os.Getenv("GOPATH"), "src")+"/", "", 1),
			frame.Line,
		)
	}
	return ""
}

func getLogLevel(lvl string) LogLevel {
	switch strings.ToLower(lvl) {
	case "debug":
		return DebugLevel
	case "warn", "warning":
		return WarnLevel
	case "error":
		return ErrorLevel
	case "fatal":
		return FatalLevel
	case "panic":
		return PanicLevel
	default:
		return InfoLevel
	}
}

func init() {
	lvl := os.Getenv("LOG_LEVEL");
	if lvl == "" {
		lvl = DefaultLevel
	}

	logger = &Logger{}
	SetOutput(os.Stdout)
	SetErrOutput(os.Stderr)
	SetLevel(DefaultLevel)
}
