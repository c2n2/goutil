/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package logger

import (
	"testing"
)

func preset() {
	SetFacility("test util")
	SetLevel("debug")
	EnableCallers()
	EnableColors()
	EnableTimestamp()
}

func TestDebugf(t *testing.T) {
	Debugf("Test debug")
}

func TestInfof(t *testing.T) {
	Infof("Test info")
}

func TestWarnf(t *testing.T) {
	Warnf("Test warn")
}

func TestErrorf(t *testing.T) {
	Errorf("Test error")
}

func init() {
	preset()
}