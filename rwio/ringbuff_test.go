/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package rwio

import (
	"testing"
)

var buf = NewRingBuffer(16)

func TestRingBuffer_Write(t *testing.T) {
	p := []byte("Hello world")
	n, err := buf.Write(p)
	if err != nil {
		t.Errorf("Write to buffer error: %v", err)
	} else {
		t.Logf("Writed %d bytes", n)
	}
}

func TestRingBuffer_Read(t *testing.T) {
	d := make([]byte, 10)
	n, err := buf.Read(d)
	if err != nil {
		t.Errorf("Read error: %v", err)
	} else {
		t.Logf("read %d bytes: %v", n, string(d))
	}
}

func BenchmarkRingBuffer_Read(b *testing.B) {

}

func BenchmarkRingBuffer_Write(b *testing.B) {

}
