/*
 * Copyright (c) 2019. Alexey Shtepa <as.shtepa@gmail.com> LICENSE MIT
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 */

package rwio

import (
	"errors"
	"sync"
	"unsafe"
)

var (
	ErrReadFromClosedBuffer = errors.New("attempt to read from closed buffer")
	ErrWriteToClosedBuffer = errors.New("attempt to write to closed buffer")
)

type bheader struct {
	data uintptr
	len  int
	cap  int
}

type RingBuffer struct {
	buff                  *unsafe.Pointer
	readIndex, writeIndex uintptr
	closed                bool
	sync.Mutex
}

// interface

func NewRingBuffer(cap int) *RingBuffer {
	return &RingBuffer{readIndex: 0, writeIndex: 0}
}

func (b *RingBuffer) Read(d []byte) (n int, err error) {
	// lock
	b.Lock();
	defer b.Unlock()
	return
}

func (b *RingBuffer) Write(p []byte) (n int, err error) {
	// lock
	b.Lock();
	defer b.Unlock()
	return
}

func (b *RingBuffer) Close() (err error) {
	b.Lock();
	defer b.Unlock()
	b.closed = true
	return
}

// stuff

/*
Available space for reading
*/
func (b *RingBuffer) avR() (sz int) {
	return 0
}

/*
Available space for writing
*/
func (b *RingBuffer) avW() (sz int) {
	return 0
}

/**/
func (b *RingBuffer) isFull() (ok bool) {
	return b.avW() == 0
}

/**/
func (b *RingBuffer) isEmpty() (ok bool) {
	return b.avR() == 0
}
