module gitlab.com/c2n2/goutil

require (
	github.com/go-pg/pg v8.0.4+incompatible
	github.com/jawher/mow.cli v1.1.0
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	mellium.im/sasl v0.2.1 // indirect
)

go 1.12
